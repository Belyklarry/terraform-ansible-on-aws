# Terraform project sample

This project sample shows the usage of the Terraform + Ansible _to be continuous_.

The project:

1. builds a basic infrastructure on AWS using Terraform,
2. then deploys an `httpd` webserver with a basic html page with project information using Ansible.

## AWS configuration

In order to connect to AWS, the project sets the required AWS credentials:

* :lock: `AWS_ACCESS_KEY_ID` - defined as secret GitLab CI variables
* :lock: `AWS_SECRET_ACCESS_KEY` - defined as secret GitLab CI variables
* `AWS_DEFAULT_REGION`

## Terraform template features

This project uses the following features from the GitLab CI Terraform template:

* enables `staging` env by declaring `TF_STAGING_ENABLED`
* enables `production` env by declaring `TF_PROD_ENABLED`
* enables [tfsec](https://github.com/tfsec/tfsec) analysis by declaring `TF_TFSEC_ENABLED`
* uses the [devwatt.tfvars](./terraform/devwatt.tfvars) variables file in all commands by declaring `TF_EXTRA_OPTS: "-var-file devwatt.tfvars"`
* passes the `ssh_pub_key_file` variable by declaring the `TF_VAR_ssh_pub_key_file` [GitLab custom variable](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui) of type file


### GitLab managed Terraform State

This project uses the [GitLab managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html) (default).

### Multi-environment support

In order to be able to manage multiple Terraform environments (`staging` & `production`), the project declares the `environment_type`, `environment_name` & `environment_slug` Terraform variables (dynamically provided by the template), and uses `environment_slug` to prefix created resource names.

Example:

```terraform
resource "aws_security_group" "webserver_sg" {
  name = "${var.environment_slug}-webserver-sg"
  ...
}
```

### Ansible inventory generation

The Terraform scripts generate the Ansible inventory using the [Terraform templating technique](https://www.linkbynet.com/produce-an-ansible-inventory-with-terraform). It is implemented with:

1. an [Ansible inventory template file](./terraform/ansible_inventory.tpl)
2. a `local_file` resource in the [outputs.tf](./terraform/outputs.tf) file to process the template and generate the inventory file in the `./tf-output` directory (stored as the job artifact).

### Terraform output variables propagation

The Ansible script requires some output variables from Terraform env creation jobs:

* `tf_public_ip`: allocated public IP
* `tf_public_dns`: allocated public DNS
* `tf_environment_name`: Terraform env name
* `tf_environment_type`: Terraform env type

This is implemented by generating a `terraform.env` file using Terraform templating:

1. a [dotenv template file](./terraform/terraform.env.tpl)
2. a `local_file` resource in the [outputs.tf](./terraform/outputs.tf) file to process the template and generate the `terraform.env` (stored as a dotenv artifact).

Therefore those variables are propagated to downstream jobs.

## Ansible template features

This project uses the following features from the GitLab CI Ansible template:

* enables `staging` env by setting `ANSIBLE_STAGING_PLAYBOOK_FILE` (to `playbook.yml`)
* enables `production` env by setting `ANSIBLE_PROD_PLAYBOOK_FILE` (to `playbook.yml`)
* passes the Ansible SSH private key by declaring the `ANSIBLE_PRIVATE_KEY` [GitLab custom variable](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui) of type file

### Dynamic environment url

As AWS is dynamically allocating floating IP, the environment url cannot be known in advance, and therefore [has to be dynamically set](https://docs.gitlab.com/ee/ci/environments/#example-of-setting-dynamic-environment-urls).

This is implemented in the [.gitlab-ci.yml](./.gitlab-ci.yml) file, reusing the `$tf_public_dns`
variable propagated through my generated dotenv file.
